# Canyon Suite Web API

This API has been created in order to provide an interface for SPAs to authenticate and manage stuff from Conquer Online Canyon Suite.


## Run Locally

Before setting up the project, download and install the following:

* [.NET 7](https://dotnet.microsoft.com/download) - Primary language compiler
* [MariaDB](https://mariadb.org/) - Recommended flavor of MySQL for project databases 
* [MySQL Workbench](https://dev.mysql.com/downloads/workbench/) - Recommended SQL editor and database importer
* [Visual Studio Code](https://code.visualstudio.com/) - Recommended editor for modifying and building project files or
* [Visual Studio Community](visualstudio.microsoft.com/vs/community/) - Also Recommended and easier for begginers


Clone the project

```bash
  git clone https://gitlab.com/world-conquer-online/canyon/canyon.api.backend
```

Restore the project

```bash
dotnet restore
```

Go to the project directory

```bash
  cd canyon.api.backend
```

Initialize the secret manager

```bash
  dotnet user-secrets init
```

Add your local environment variables

```bash
dotnet user-secrets set "OAuth:Url" "http://localhost:8080"
dotnet user-secrets set "OAuth:Authority" "http://localhost:8080"
dotnet user-secrets set "OAuth:RedirectUrl" "https://localhost:4200"
dotnet user-secrets set "OAuth:Realm" "your-realm"
dotnet user-secrets set "OAuth:Audience" "your-audience"
dotnet user-secrets set "OAuth:Account:ClientId" "your-client-id"
dotnet user-secrets set "OAuth:Account:ClientSecret" "your-client-secret"
```

Start the server pressing F5 on your Visual Studio


## Environment Variables

To run this project, you will need to add the following environment variables to your system or application.json file.

If you're using Linux or Mac as deployment system use __ (double underline). Example: `OAuth:Url` will be `OAuth__Url`.

`ConnectionStrings:DefaultConnection` connection string for MySQL connection with account database.

`OAuth:Url` is the domain where Keycloak is located.

`OAuth:Realm` the name of the keycloak realm you're going to authenticate at.

`OAuth:Audience` the keycloak audience to be validated in the token.

`OAuth:Account:ClientId` the client id for the client which will be used for authentication and authorization.

`OAuth:Account:ClientSecret` the client secret for the client which will be used for authentication and authorization.
# Authors

* Felipe Vieira Vendramini [Konichu]