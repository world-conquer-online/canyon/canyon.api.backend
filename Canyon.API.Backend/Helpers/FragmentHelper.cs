﻿namespace Canyon.API.Backend.Helpers
{
    public static class FragmentHelper
    {
        public static Dictionary<string, string> ParseFragment(this string fragment)
        {
            var fragmentData = new Dictionary<string, string>();
            var fragments = fragment.Split(new[] { '&', '#' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var fragmentPart in fragments)
            {
                var keyValue = fragmentPart.Split('=');
                if (keyValue.Length == 2)
                {
                    fragmentData[keyValue[0]] = keyValue[1];
                }
            }

            return fragmentData;
        }
    }
}
