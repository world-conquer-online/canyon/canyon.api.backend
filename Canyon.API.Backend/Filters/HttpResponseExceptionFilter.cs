﻿using Canyon.API.Backend.Models.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Canyon.API.Backend.Filters
{
    public class HttpResponseExceptionFilter : IActionFilter, IOrderedFilter
    {
        public int Order => int.MaxValue - 10;

        public void OnActionExecuting(ActionExecutingContext context) { }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception is HttpExceptionBase httpException)
            {
                context.Result = new ObjectResult(new HttpExceptionResponse(httpException.Path, httpException.Error, httpException.Timestamp))
                {
                    StatusCode = httpException.StatusCode
                };
                context.ExceptionHandled = true;
            }
            else if (context.Exception != null)
            {
                context.Result = new ObjectResult(new HttpExceptionResponse(context.HttpContext.Request.Path.Value, context.Exception.Message, DateTimeOffset.Now))
                {
                    StatusCode = 500
                };
                context.ExceptionHandled = true;
            }
        }
    }
}
