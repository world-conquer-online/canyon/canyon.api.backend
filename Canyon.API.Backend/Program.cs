using Canyon.API.Backend.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Reflection;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Protocols;
using Canyon.API.Backend.Models.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Canyon.API.Backend.Services.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using System.Security.Claims;
using Canyon.API.Backend.Services;
using Microsoft.IdentityModel.Tokens;
using Canyon.API.Backend.Filters;

namespace Canyon.API.Backend
{
    public class Program
    {
        public static string Version => Assembly.GetEntryAssembly()?
                                                .GetCustomAttribute<AssemblyInformationalVersionAttribute>()?
                                                .InformationalVersion ?? "0.0.0.0";

        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            ConfigurationManager configuration = builder.Configuration;
            string connectionString = configuration.GetConnectionString("DefaultConnection");
            builder.Services.AddDbContext<ApplicationDbContext>(options =>
                options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString)));

            builder.Services.AddHttpClient();
            builder.Services.AddDefaultIdentity<ApplicationUser>(opt =>
            {
                opt.Password.RequireDigit = true;
                opt.Password.RequireLowercase = true;
                opt.Password.RequireNonAlphanumeric = false;
                opt.Password.RequireUppercase = true;
                opt.Password.RequiredLength = 6;
                opt.Password.RequiredUniqueChars = 1;

                // Lockout settings.
                opt.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(15);
                opt.Lockout.MaxFailedAccessAttempts = 5;
                opt.Lockout.AllowedForNewUsers = true;

                // User settings.
                opt.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                opt.User.RequireUniqueEmail = true;
                opt.SignIn.RequireConfirmedEmail = false;
                opt.SignIn.RequireConfirmedAccount = false;
                opt.SignIn.RequireConfirmedPhoneNumber = false;
            })
                   .AddRoles<ApplicationRole>()
                   .AddUserManager<ApplicationUserManager>()
                   .AddRoleManager<ApplicationRoleManager>()
                   .AddUserStore<ApplicationUserStore>()
                   .AddRoleStore<RoleStore<ApplicationRole, ApplicationDbContext, Guid, ApplicationUserRole, ApplicationRoleClaim>>()
                   .AddSignInManager<ApplicationSignInManager>()
                   .AddDefaultTokenProviders();

            builder.Services.AddControllers(opt =>
            {
                opt.Filters.Add<HttpResponseExceptionFilter>();
            });
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(opt =>
            {
                opt.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = configuration["Swagger:Title"],
                    Description = configuration["Swagger:Description"],
                    Version = Version,
                    Contact = new OpenApiContact
                    {
                        Email = configuration["Swagger:Contact:Email"],
                        Name = configuration["Swagger:Contact:Name"]
                    }
                });

                opt.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please enter a valid token",
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    BearerFormat = "JWT",
                    Scheme = "Bearer"
                });

                opt.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type=ReferenceType.SecurityScheme,
                                Id="Bearer"
                            }
                        },
                        new string[]{}
                    }
                });

                string xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                opt.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
            });

            builder.Services.AddAuthentication("Bearer")
                .AddJwtBearer("Bearer", options =>
                {
                    options.ConfigurationManager = new ConfigurationManager<OpenIdConnectConfiguration>($"{configuration["OAuth:Url"]}/realms/{configuration["OAuth:Realm"]}/.well-known/openid-configuration", new OpenIdConnectConfigurationRetriever());
                    options.Authority = configuration["OAuth:Authority"];
                    options.Audience = configuration["OAuth:Audience"];
                    options.ClaimsIssuer = $"{configuration["OAuth:Url"]}/realms/{configuration["OAuth:Realm"]}";
#if DEBUG
                    options.IncludeErrorDetails = true;
#endif
                });

            RegisterServices(builder.Services);

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();
            app.UseAuthorization();

            app.MapControllers();
#if DEBUG
            app.UseCors();
#endif

            app.Run();
        }

        private static void RegisterServices(IServiceCollection service)
        {
            service.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            service.AddTransient(s =>
            {
                IHttpContextAccessor contextAccessor = s.GetService<IHttpContextAccessor>();
                ClaimsPrincipal user = contextAccessor?.HttpContext?.User;
                return user;
            });

            service.AddTransient<IEmailSender, EmailService>();
            service.AddTransient<IPasswordHasher<ApplicationUser>, WhirlpoolPasswordService>();

#if DEBUG
            service.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                {
                    builder.WithOrigins("https://localhost:4200")
                           .AllowAnyHeader()
                           .AllowAnyMethod();
                });
            });
#endif
        }
    }
}