﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Canyon.API.Backend.Controllers
{
    [Route("api/registration/[action]")]
    [ApiController]

    public class RegistrationController : ControllerBase
    {
        private readonly ILogger<RegistrationController> logger;

        public RegistrationController(
            ILogger<RegistrationController> logger
            )
        {
            this.logger = logger;
        }

        [HttpPost]
        [ActionName("account")]
        public async Task<IActionResult> OnPostNewAccountAsync()
        {

            return Ok();
        }

        [Authorize]
        [HttpPost]
        [ActionName("game-account")]
        public async Task<IActionResult> OnPostNewGameAccountAsync()
        {

            return Ok();
        }
    }
}
