﻿using Canyon.API.Backend.Models.Authentication;
using Canyon.API.Backend.Models.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Canyon.API.Backend.Controllers
{
    [Route("api/authentication/[action]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly HttpClient httpClient;
        private readonly IConfiguration configuration;
        private readonly ILogger<AuthenticationController> logger;

        public AuthenticationController(HttpClient httpClient,
            IConfiguration configuration,
            ILogger<AuthenticationController> logger)
        {
            this.httpClient = httpClient;
            this.configuration = configuration;
            this.logger = logger;
        }

        /// <summary>
        /// Authenticates the user in the system using a Keycloak account or federated from Canyon
        /// Database.
        /// </summary>
        [ActionName("sign-in")]
        [HttpPost]
        [ProducesResponseType(typeof(AuthenticationResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(HttpExceptionResponse), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(HttpExceptionResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(HttpExceptionResponse), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> OnAuthorizationCodeCallbackAsync([FromBody] AuthenticationRequest body)
        {
            if (string.IsNullOrEmpty(body.Code))
            {
                logger.LogWarning("[{}] Attempt to authenticate with null or empty code.", HttpContext.Connection.RemoteIpAddress);
                throw new BadRequestException(Request.Path.Value, "Field 'code' cannot be null.");
            }

            var authBody = new[]
            {
                new KeyValuePair<string, string>("grant_type", "authorization_code"),
                new KeyValuePair<string, string>("code", body.Code),
                new KeyValuePair<string, string>("client_id", configuration["OAuth:Account:ClientId"]),
                new KeyValuePair<string, string>("client_secret", configuration["OAuth:Account:ClientSecret"]),
                new KeyValuePair<string, string>("redirect_uri", configuration["OAuth:RedirectUrl"])
            };

            var response = await httpClient.PostAsync($"{configuration["OAuth:Url"]}/realms/{configuration["OAuth:Realm"]}/protocol/openid-connect/token", new FormUrlEncodedContent(authBody));
            if (!response.IsSuccessStatusCode)
            {
                logger.LogWarning("[{}] Authentication did not succeed! Status Code: {}\n{}", HttpContext.Connection.RemoteIpAddress, response.StatusCode, await response.Content.ReadAsStringAsync());
                throw new UnauthorizedException(Request.Path.Value, "Failed to authenticate.");
            }

            AuthenticationResponse authenticationResponse = await response.Content.ReadFromJsonAsync<AuthenticationResponse>();
            if (string.IsNullOrEmpty(authenticationResponse.AccessToken))
            {
                logger.LogWarning("[{}] Keycloak returned empty or null access token.", HttpContext.Connection.RemoteIpAddress);
                throw new UnauthorizedException(Request.Path.Value, "Invalid authentication result.");
            }

            logger.LogInformation("[{}] Client authenticated with success", HttpContext.Connection.RemoteIpAddress);
            return StatusCode(201, authenticationResponse);
        }

        /// <summary>
        /// Refresh the user token for a longer session time.
        /// </summary>
        [ActionName("refresh")]
        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(AuthenticationResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(HttpExceptionResponse), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(HttpExceptionResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(HttpExceptionResponse), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> OnRefreshTokenCallbackAsync([FromBody] RefreshTokenRequest body)
        {
            if (string.IsNullOrEmpty(body.RefreshToken))
            {
                logger.LogWarning("[{}] Attempt to refresh token with null or empty token.", HttpContext.Connection.RemoteIpAddress);
                throw new BadRequestException(Request.Path.Value, "Field 'refresh_token' cannot be null.");
            }

            var authBody = new[]
           {
                new KeyValuePair<string, string>("grant_type", "refresh_token"),
                new KeyValuePair<string, string>("refresh_token", body.RefreshToken),
                new KeyValuePair<string, string>("client_id", configuration["OAuth:Account:ClientId"]),
                new KeyValuePair<string, string>("client_secret", configuration["OAuth:Account:ClientSecret"])
            };

            var response = await httpClient.PostAsync($"{configuration["OAuth:Url"]}/realms/{configuration["OAuth:Realm"]}/protocol/openid-connect/token", new FormUrlEncodedContent(authBody));
            if (!response.IsSuccessStatusCode)
            {
                logger.LogWarning("[{}] Authentication did not succeed! Status Code: {}", HttpContext.Connection.RemoteIpAddress, response.StatusCode);
                throw new UnauthorizedException(Request.Path.Value, "Failed to authenticate.");
            }

            AuthenticationResponse authenticationResponse = await response.Content.ReadFromJsonAsync<AuthenticationResponse>();
            if (string.IsNullOrEmpty(authenticationResponse.AccessToken))
            {
                logger.LogWarning("[{}] Keycloak returned empty or null access token.", HttpContext.Connection.RemoteIpAddress);
                throw new UnauthorizedException(Request.Path.Value, "Invalid authentication result.");
            }

            logger.LogInformation("[{}] Client authenticated with success", HttpContext.Connection.RemoteIpAddress);
            return StatusCode(201, authenticationResponse);
        }
    }
}
