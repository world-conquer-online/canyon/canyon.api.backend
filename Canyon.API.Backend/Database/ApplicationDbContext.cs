﻿using Canyon.API.Backend.Models.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Duende.IdentityServer.EntityFramework.Entities;
using Duende.IdentityServer.EntityFramework.Interfaces;
using Duende.IdentityServer.EntityFramework.Options;
using Duende.IdentityServer.EntityFramework.Extensions;

namespace Canyon.API.Backend.Database
{
    public class ApplicationDbContext : IdentityDbContext<
        ApplicationUser,
        ApplicationRole,
        Guid,
        ApplicationUserClaim,
        ApplicationUserRole,
        ApplicationUserLogin,
        ApplicationRoleClaim,
        ApplicationUserToken
        >, IPersistedGrantDbContext
    {
        private readonly ILogger<ApplicationDbContext> logger;
        private readonly IOptions<OperationalStoreOptions> operationalStoreOptions;

        public ApplicationDbContext(
            DbContextOptions<ApplicationDbContext> options,
            IOptions<OperationalStoreOptions> operationalStoreOptions,
            ILogger<ApplicationDbContext> logger)
            : base(options)
        {
            this.logger = logger;
            this.operationalStoreOptions = operationalStoreOptions;
        }

        /// <summary>
        ///     Gets or sets the <see cref="DbSet{PersistedGrant}" />.
        /// </summary>
        public virtual DbSet<PersistedGrant> PersistedGrants { get; set; }

        /// <summary>
        ///     Gets or sets the <see cref="DbSet{DeviceFlowCodes}" />.
        /// </summary>
        public virtual DbSet<DeviceFlowCodes> DeviceFlowCodes { get; set; }
        public virtual DbSet<Key> Keys { get; set; }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ApplicationUserToken>().HasKey(x => new { x.UserId, x.LoginProvider, x.Name });
            builder.Entity<ApplicationUserRole>().HasKey(x => new { x.UserId, x.RoleId });
            builder.Entity<ApplicationUserLogin>().HasKey(x => new { x.LoginProvider, x.ProviderKey });

            builder.ConfigurePersistedGrantContext(operationalStoreOptions.Value);
        }

        Task<int> IPersistedGrantDbContext.SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }
    }
}
