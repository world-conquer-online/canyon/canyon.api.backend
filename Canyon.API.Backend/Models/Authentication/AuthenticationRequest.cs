﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Canyon.API.Backend.Models.Authentication
{
    public class AuthenticationRequest
    {
        [Required]
        [JsonPropertyName("code")]
        public string Code { get; set; }
    }
}
