﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Canyon.API.Backend.Models.Authentication
{
    public class RefreshTokenRequest
    {
        [Required]
        [JsonPropertyName("refresh_token")]
        public string RefreshToken { get; set; }
    }
}
