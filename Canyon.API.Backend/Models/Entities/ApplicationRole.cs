﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Canyon.API.Backend.Models.Entities
{
    [Table("account_roles")]
    public class ApplicationRole : IdentityRole<Guid>
    {
        [Key]
        public override Guid Id
        {
            get => base.Id;
            set => base.Id = value;
        }

        public DateTime CreationDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
