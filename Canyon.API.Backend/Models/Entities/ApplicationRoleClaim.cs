﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Canyon.API.Backend.Models.Entities
{
    [Table("account_role_claims")]
    public class ApplicationRoleClaim : IdentityRoleClaim<Guid>
    {
        [Key]
        public new Guid Id { get; set; }

        [ForeignKey("FK_AspNetRoleClaims_AspNetRoles_RoleId")]
        public override Guid RoleId { get; set; }
    }
}
