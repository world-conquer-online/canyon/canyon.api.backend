﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Canyon.API.Backend.Models.Entities
{
    [Table("account_user_roles")]
    public class ApplicationUserRole : IdentityUserRole<Guid>
    {
        [Key]
        [ForeignKey("FK_AspNetUserRoles_AspNetUsers_UserId")]
        public override Guid UserId { get; set; }

        [Key]
        [ForeignKey("FK_AspNetUserRoles_AspNetRoles_RoleId")]
        public override Guid RoleId { get; set; }

        [Column] public DateTime CreationDate { get; set; }
        [Column] public DateTime? ModifiedDate { get; set; }
    }
}
