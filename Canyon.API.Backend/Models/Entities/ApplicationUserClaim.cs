﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Canyon.API.Backend.Models.Entities
{
    [Table("account_user_claims")]
    public class ApplicationUserClaim : IdentityUserClaim<Guid>
    {
        [Key]
        public new Guid Id { get; set; }

        [ForeignKey("FK_AspNetUserClaims_AspNetUsers_UserId")]
        public override Guid UserId { get; set; }
    }
}
