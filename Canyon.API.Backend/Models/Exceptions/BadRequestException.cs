﻿using System.Net;

namespace Canyon.API.Backend.Models.Exceptions
{
    public sealed class BadRequestException : HttpExceptionBase
    {
        public BadRequestException(string path, string error) : base(HttpStatusCode.BadRequest, path, error)
        {
        }

        public BadRequestException(string path, Exception ex) : base(HttpStatusCode.BadRequest, path, ex)
        {
        }
    }
}
