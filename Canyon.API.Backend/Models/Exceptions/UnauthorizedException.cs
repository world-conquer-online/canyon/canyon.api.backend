﻿using System.Net;

namespace Canyon.API.Backend.Models.Exceptions
{
    public sealed class UnauthorizedException : HttpExceptionBase
    {
        public UnauthorizedException(string path, string error) : base(HttpStatusCode.Unauthorized, path, error)
        {
        }

        public UnauthorizedException(string path, Exception ex) : base(HttpStatusCode.Unauthorized, path, ex)
        {
        }
    }
}
