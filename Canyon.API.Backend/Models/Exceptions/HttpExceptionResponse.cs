﻿using System.ComponentModel;
using System.Net;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Canyon.API.Backend.Models.Exceptions
{
    
    public class HttpExceptionResponse
    {
        public HttpExceptionResponse(string path, string error, DateTimeOffset timestamp)
        {
            Path = path;
            Error = error;
            Timestamp = DateTimeOffset.UtcNow;
        }

        [DefaultValue("/api/example/url")]
        public string Path { get; }
        [DefaultValue("An error has occured.")]
        public string Error { get; }
        public DateTimeOffset Timestamp { get; }
    }
}
