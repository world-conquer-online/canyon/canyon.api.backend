﻿using Microsoft.AspNetCore.Identity.UI.Services;
using MimeKit.Text;
using MimeKit;
using MailKit.Net.Smtp;

namespace Canyon.API.Backend.Services
{
    public class EmailService : IEmailSender
    {
        private readonly ILogger<EmailService> logger;
        private readonly EmailSettings settings;

        public EmailService(ILogger<EmailService> logger, IConfiguration configuration)
        {
            settings = new EmailSettings
            {
                PrimaryDomain = configuration["DontReplyMail:PrimaryDomain"],
                PrimaryPort = int.Parse(configuration["DontReplyMail:PrimaryPort"]),
                UsernameEmail = configuration["DontReplyMail:UsernameEmail"],
                UsernamePassword = configuration["DontReplyMail:UsernamePassword"],
                FromEmail = configuration["DontReplyMail:FromEmail"],
                EnableSsl = bool.Parse(configuration["DontReplyMail:EnableSsl"]),
                FromName = configuration["DontReplyMail:FromName"]
            };

            this.logger = logger;
        }

        public async Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(settings.FromName, settings.FromEmail));
            message.To.Add(new MailboxAddress(email, email));
            message.Subject = subject;
            message.Body = new TextPart(TextFormat.Html)
            {
                Text = htmlMessage
            };
            try
            {
                using var client = new SmtpClient();
                /*
                 * Keep this lines if using Selfsigned certificate (or no certificate at all)
                 */
                client.ServerCertificateValidationCallback = (s, c, h, e) => true; //NOSONAR
                client.CheckCertificateRevocation = false;                         //NOSONAR

                await client.ConnectAsync(settings.PrimaryDomain, settings.PrimaryPort);
                await client.AuthenticateAsync(settings.UsernameEmail, settings.UsernamePassword);
                await client.SendAsync(message);
                await client.DisconnectAsync(true);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
        }

        public class EmailSettings
        {
            public string PrimaryDomain { get; set; }
            public int PrimaryPort { get; set; }
            public string UsernameEmail { get; set; }
            public string UsernamePassword { get; set; }
            public string FromName { get; set; }
            public string FromEmail { get; set; }
            public bool EnableSsl { get; set; }
        }
    }
}
