﻿using Canyon.API.Backend.Database;
using Canyon.API.Backend.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Canyon.API.Backend.Services.Identity
{
    public class ApplicationRoleManager : RoleManager<ApplicationRole>
    {
        private readonly ApplicationDbContext context;

        public ApplicationRoleManager(IRoleStore<ApplicationRole> store,
                           IEnumerable<IRoleValidator<ApplicationRole>> roleValidators,
                           ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors,
                           ILogger<RoleManager<ApplicationRole>> logger,
                           ApplicationDbContext ctx)
            : base(store, roleValidators, keyNormalizer, errors, logger)
        {
            context = ctx;
        }

        public Task<List<ApplicationRole>> GetRolesAsync(int page, int ipp = 10,
                                                         CancellationToken cancellationToken = default)
        {
            ThrowIfDisposed();
            cancellationToken.ThrowIfCancellationRequested();

            return context.Roles.Skip(page * ipp).Take(ipp).ToListAsync(cancellationToken);
        }

        public Task<ApplicationRoleClaim> GetRoleClaimAsync(Guid role, string claim,
                                                            CancellationToken cancellationToken = default)
        {
            ThrowIfDisposed();
            cancellationToken.ThrowIfCancellationRequested();

            return context.RoleClaims.FirstOrDefaultAsync(x => x.RoleId == role && x.ClaimType.Equals(claim),
                                                           cancellationToken);
        }
    }
}
