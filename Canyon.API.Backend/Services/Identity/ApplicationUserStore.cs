﻿using Canyon.API.Backend.Database;
using Canyon.API.Backend.Models.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Canyon.API.Backend.Services.Identity
{
    public class ApplicationUserStore : UserStore<
        ApplicationUser,
        ApplicationRole,
        ApplicationDbContext,
        Guid,
        ApplicationUserClaim,
        ApplicationUserRole,
        ApplicationUserLogin,
        ApplicationUserToken,
        ApplicationRoleClaim
    >
    {
        private readonly ILogger<ApplicationUserStore> logger;

        public ApplicationUserStore(ILogger<ApplicationUserStore> logger,
                                    ApplicationDbContext dbContext)
            : base(dbContext)
        {
            this.logger = logger;
        }

        public override async Task<IdentityResult> CreateAsync(ApplicationUser user,
                                                               CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            try
            {
                if (await Context.Users.AnyAsync(usr => usr.NormalizedEmail.Equals(user.NormalizedEmail),
                                                 cancellationToken))
                    return IdentityResult.Failed(new IdentityError
                    {
                        Code = "EMAIL_IN_USE",
                        Description = "This choosen e-mail address is already in use."
                    });

                if (await Context.Users.AnyAsync(usr => usr.NormalizedUserName.Equals(user.NormalizedUserName),
                                                 cancellationToken))
                    return IdentityResult.Failed(new IdentityError
                    {
                        Code = "USERNAME_ALREADY_EXISTS",
                        Description = "The choosen username is already in use."
                    });

                Context.Users.Add(user);
                await Context.SaveChangesAsync(cancellationToken);
                return IdentityResult.Success;
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "{0}", ex.Message);
                return IdentityResult.Failed(new IdentityError
                {
                    Code = ex.HResult.ToString("X08"),
                    Description = ex.InnerException.Message
                });
            }
        }
    }
}
